//
//  ViewController.swift
//  Less3
//
//  Created by Yaroslav Georgievich on 11.07.2018.
//  Copyright © 2018 Yaroslav Georgievich. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //maxNumber(firstNumber: 10,secondNumber: 20)
        //squareAndCubeNumber(firstNumber: 10)
        //numberRange(number: 13)
        //numberDevider(number: 52)
        //numberIsPerfect(number: 28)
        
        //accBalance(ageBuy: 1826, ageNow: 2018, interestRate: 0.06)
        //moneyOnYear(scholarship: 700, interestRate: 0.03, month: 10)
        //deathAfter(scholarship: 700, interestRate: 0.03)
        changeNumber(inputNumber: 25)
        
    }
    //DZ_1_0  Вывести наибольшее число
    func maxNumber(firstNumber: Int,secondNumber: Int) {
        if firstNumber > secondNumber {
            print(firstNumber, " больше")
        }
        else if firstNumber < secondNumber {
            print(secondNumber, " больше")
        }
        else {
            print("Они равны")
        }
    }
    //DZ_1_1 Вывести квадрат и куб числа
    func squareAndCubeNumber(firstNumber: Double) {
        let square = firstNumber * firstNumber
        print("Квадрат числа: ", square)
        let cube = pow(firstNumber, 3)
        print("Куб числа: ", cube)
    }
    //DZ_1_2 Вывести все числа до заданого и обратно
    func numberRange(number: Int) {
        //Цикл до number что бы убрать повторения числа 13
        for i in 0..<number {
            print(i)
        }
        //Цикл до number + 1 что бы число заканчивалось на 0
        for j in 0..<(number + 1) {
            let j = number - j
            print(j)
        }
    }
    //DZ_1_3 Колово делителей и вывести их
    func numberDevider(number: Int) {
        var j = 0
        //Цикл начинаеться с 1 потому как делитель не может равняться 0
        for i in 1..<number {
            //остаток от деленния
            let b = number%i
            if b == 0 {
                j += 1
                print("Делитель: ", i)
            }
        }
        print("Колово делителей: ", j)
    }
    //DZ_1_4 Проверить является число совершенным и вывести его делители
    func numberIsPerfect(number: Int) {
        var sum = 0
        //Цикл начинаеться с 1 потому как делитель не может равняться 0
        for i in 1..<number {
            //остаток от деленния
            let b = number%i
            if b == 0 {
                sum = sum + i
                print("делители", i)
            }
        }
        
        if sum == number {
            print("Число", number, "совершенное!")
        }
        else {
            print("Число", number, "не являеться совершенным!")
        }
    }
    //DZ_2_1 Посчитать суму счета
    func accBalance(ageBuy: Int,
                    ageNow: Int,
                    interestRate: Double) {
        var costIsland = 24.0
        let agePassed = ageNow - ageBuy
        
        for _ in 0..<agePassed {
            costIsland = costIsland + costIsland * interestRate
        }
        print("Account balance: ", costIsland)
    }
    //DZ_2_2 Найти необходимую сумму на учебный год
    func moneyOnYear(interestRate: Double, month: Int) ->Double {
        var expenses = 1000.0
        var needMoney = 0.0
        
        for _ in 0..<month {
            expenses = expenses + expenses * interestRate
            needMoney = needMoney + expenses
        }
        print("Необходимая сумма на 10 месяцев: ", needMoney)
        return needMoney
    }
    //DZ_2_3 Сколько месяцев сможет прожить студент
    func deathAfter(scholarship: Int, interestRate: Double) -> Int {
        var expenses = 1000.0
        var balance = 2400.0
        var month = 0
        
        while balance > 0 {
            expenses = expenses + expenses * interestRate
            balance = balance + Double(scholarship) - expenses
            month += 1
        }
        print("Cможет прожить: ", month, " месяцев")
        return month
    }
    //DZ_2_4 Число в обратном порядке
    func changeNumber(inputNumber: Int) -> Int {
        let fourth = (inputNumber%10000) / 1000
        let third = (inputNumber%1000) / 100
        let second = (inputNumber%100) / 10
        let first = (inputNumber%10)
        var result = 0
        
        if fourth == 0 {
            if third == 0 {
                result = 10 * first + second
                print(result)
            }
            else {
                result = 100 * first + 10 * second + third
                print(result)
            }
        }
        else {
            result = 1000 * first + 100 * second + 10 * third + fourth
            print(result)
        }
        return result
    }
}

